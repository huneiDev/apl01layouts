package lt.matas.layouts;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {


    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar)findViewById(R.id.loadingBar);


        setLoading(true);

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                setLoading(false);

            }
        },10000); // Palauks 10 sekundziu, ir ivykdys run


    }

    void setLoading(boolean state){

        if(state){ // Padarom, kad jis loadintusi

            progressBar.setVisibility(View.VISIBLE);

        }else{

            progressBar.setVisibility(View.INVISIBLE);

        }

    }

}
